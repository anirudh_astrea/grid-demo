import { useEffect, useState } from "react";
import "./App.css";

function App() {
  const [arr, setArr] = useState([1, 2, 3]);

  useEffect(() => {
    const temp = [...arr];
    if (temp.length < 8) {
      while (temp.length < 8) temp.push(...arr);
    }
    setArr(temp);
  }, []);

  const modifyArr = () => {
    let temp = arr.slice(8);
    temp = [...temp, ...arr.slice(0, 8)];
    console.log(temp);
    setArr(temp);
  };

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
        height: "100%",
        backgroundColor: "black",
      }}
    >
      <div
        style={{
          display: "flex",
          flexWrap: "wrap",
          flexDirection: "row",
          height: 630,
          width: 630,
          border: "3px solid red",
          padding: 5,
        }}
      >
        {arr &&
          arr.slice(0, 8).map((ele, index) => (
            <div
              key={index}
              style={{
                width: 200,
                height: 200,
                backgroundColor: "yellow",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                margin: 5,
              }}
            >
              {ele}
            </div>
          ))}
        <button
          style={{
            width: 200,
            height: 200,
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            margin: 5,
            border: "none",
            background: "#ffffff2a",
          }}
          onClick={() => {
            modifyArr();
          }}
        >
          <span style={{ fontSize: 72, color: "white" }}>+</span>
        </button>
      </div>
    </div>
  );
}

export default App;
